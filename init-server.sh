#!/usr/bin/env bash

pip install -r requirements.txt
python manage.py makemigrations
python manage.py makemigrations api
python manage.py migrate
python manage.py loaddata auth cards taxes reservations transactions
