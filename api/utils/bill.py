# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from hashlib import md5
from datetime import datetime
from time import time

import base64
from django.conf import settings

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256


PRODUCT_KEY = '78111808'
SERVICE_UNIT = 'Unidad de servicio'
SERVICE_KEY = 'E48'
CONCEPT_DESCRIPTION = 'SERVICIO PRIVADO DE TRANSPORTE CON CHOFER'

BASE_DIR = 'bills'


def cipher(key, source):
    key = SHA256.new(key.encode('utf-8')).digest()
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    remaining = len(source) % AES.block_size
    if remaining != 0:
        padding = AES.block_size - remaining
        source += chr(padding) * padding
    data = cipher.encrypt(source)
    return base64.b64encode(iv + data)

def decrypt(key, source):
    key = SHA256.new(key.encode('utf-8')).digest()
    enc = base64.b64decode(source)
    iv = enc[:AES.block_size]
    decipher = AES.new(key, AES.MODE_CBC, iv)
    data = decipher.decrypt(enc[16:])
    padding = ord(data[-1])
    if padding < 16:
        if data[-padding:] != chr(padding) * padding:
            raise ValueError("Invalid padding")
        data = data[:-padding]
    return data


def generate_concepts(
        quantity, unit_price, product_key=PRODUCT_KEY,
        service_unit=SERVICE_UNIT, service_key=SERVICE_KEY,
        description=CONCEPT_DESCRIPTION,
):
    unit_price = float(unit_price) * 100 / 116
    total = quantity * unit_price
    unit_price = round(unit_price, 2)
    taxes = round(total * 0.16, 2)
    total = round(total, 2)
    concept = '\n\t\t<cfdi:Concepto ClaveProdServ="{ClaveProdServ}" Unidad="{Unidad}" ClaveUnidad="{ClaveUnidad}" Cantidad="{Cantidad}" Descripcion="{Descripcion}" ValorUnitario="{ValorUnitario}" Importe="{Importe}">' \
              '\n\t\t\t<cfdi:Impuestos>' \
              '\n\t\t\t\t<cfdi:Traslados>' \
              '\n\t\t\t\t\t<cfdi:Traslado Base="{Traslados_Base}" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="{Traslados_Importe}"/>' \
              '\n\t\t\t\t</cfdi:Traslados>' \
              '\n\t\t\t</cfdi:Impuestos>' \
              '\n\t\t</cfdi:Concepto>'.format(
        ClaveProdServ=product_key,
        Unidad=service_unit,
        ClaveUnidad=service_key,
        Cantidad=quantity,
        Descripcion=description,
        ValorUnitario=unit_price,
        Importe=total,
        Traslados_Base=total,
        Traslados_Importe=taxes
    )
    return concept, total, taxes


def generate_xml(folio, rfc, name, rides):
    concepts = ''
    subtotal = 0
    taxes = 0
    for ride in rides:
        concept, tmp_subtotal, tmp_taxes = generate_concepts(ride['seats'], ride['unit_price'])
        concepts += concept
        subtotal += tmp_subtotal
        taxes += tmp_taxes

    base = '<cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" Version="3.3" Serie="A" Folio="{Folio}" Fecha="{Fecha}" FormaPago="04" NoCertificado="00001000000406738300" SubTotal="{SubTotal}" Moneda="MXN" Total="{Total}" TipoDeComprobante="I" MetodoPago="PUE" LugarExpedicion="06760" Certificado="dasdsadas">' \
           '\n\t<cfdi:Emisor Rfc="{Emisor_Rfc}" Nombre="{Emisor_Nombre}" RegimenFiscal="{Emisor_RegimenFiscal}"/>' \
           '\n\t<cfdi:Receptor Rfc="{Receptor_Rfc}" Nombre="{Receptor_Nombre}" UsoCFDI="{Receptor_UsoCFDI}"/>' \
           '\n\t<cfdi:Conceptos>{concepts}\n\t</cfdi:Conceptos>' \
           '\n\t<cfdi:Impuestos TotalImpuestosTrasladados="{TotalImpuestosTrasladados}">' \
           '\n\t\t<cfdi:Traslados>' \
           '\n\t\t\t<cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="{Importe}"/>' \
           '\n\t\t</cfdi:Traslados>' \
           '\n\t</cfdi:Impuestos>' \
           '\n</cfdi:Comprobante>'.format(
        Folio=folio,
        Fecha=datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
        SubTotal=subtotal,
        Total=subtotal+taxes,
        Emisor_Rfc='AAXX010101000',
        Emisor_Nombre='Urbvan',
        Emisor_RegimenFiscal='601',
        Receptor_Rfc=rfc,
        Receptor_Nombre=name,
        Receptor_UsoCFDI='G03',
        concepts=concepts,
        TotalImpuestosTrasladados=taxes,
        Importe=taxes
    )

    encrypted = cipher(settings.SECRET_KEY, base)
    file_name = md5(str(time())).hexdigest()[:36] + '.xml'
    file_path = os.path.join(settings.MEDIA_ROOT, BASE_DIR, file_name)
    with open(file_path, 'wb') as f:
        f.write(encrypted)
    return BASE_DIR, file_name
