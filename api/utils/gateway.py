# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from hashlib import md5
from random import random
from time import time


def create_card(**kwargs):
    """
    Simulates connection with gateway in order to create a card
    :param holder:
    :param card_number:
    :param exp_month:
    :param exp_year:
    :param cvv:
    :return:
    """
    id = ("card_" + md5(str(time())).hexdigest())[:30]
    card_type = 'debit' if random() >= 0.5 else 'credit'
    brand = 'Mastercard' if random() >= 0.5 else 'Visa'
    card = {
        'id': id,
        'type': card_type,
        'brand': brand,
        'country': 'MX'
    }
    return card