# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.validators import MinLengthValidator
from django.db import models

from api.models.card import Card


class TaxProfile(models.Model):
    """"""
    legal_name = models.CharField(max_length=64)
    state = models.CharField(max_length=64)
    city = models.CharField(max_length=64)
    district = models.CharField(max_length=64)
    neighbourhood = models.CharField(max_length=64)
    street = models.CharField(max_length=64)
    street_ext = models.CharField(max_length=64)
    street_int = models.CharField(max_length=64)
    postcode = models.CharField(max_length=5)
    rfc = models.CharField(
        max_length=13,
        validators=[MinLengthValidator(12)],
        db_index=True
    )
    card = models.ForeignKey(
        Card,
        related_name='taxes',
        db_index=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='taxes',
        db_index=True
    )

    class Meta:
        unique_together = ("card", "user",)

    def __unicode__(self):
        return self.rfc
