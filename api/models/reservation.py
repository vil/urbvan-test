# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models

class Route(models.Model):
    """"""
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return self.name


class Stop(models.Model):
    """"""
    address = models.TextField()
    name = models.CharField(max_length=32)
    routes = models.ManyToManyField(
        Route,
        through='RouteStop',
        related_name='stops',
    )

    def __unicode__(self):
        return self.name


class RouteStop(models.Model):
    route = models.ForeignKey(
        Route
    )
    stop = models.ForeignKey(
        Stop
    )
    order = models.IntegerField()

    class Meta:
        ordering = ['order']


class Reservation(models.Model):
    """"""
    client = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='reservations',
        db_index=True
    )
    unit_price = models.DecimalField(max_digits=6, decimal_places=2)
    seats = models.SmallIntegerField()
    pickup = models.ForeignKey(
        Stop,
        related_name='reservations_pickup',
        db_index=True
    )
    dropoff = models.ForeignKey(
        Stop,
        related_name='reservations_dropoff',
        db_index=True
    )
    route = models.ForeignKey(
        Route,
    )
    billed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    traveled_at = models.DateTimeField()

    def __unicode__(self):
        return self.route.__unicode__()
