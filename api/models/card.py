# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.validators import (
    MaxValueValidator,
    MinLengthValidator,
    MinValueValidator
)
from django.db import models


class Card(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    brand = models.CharField(max_length=10)
    country = models.CharField(max_length=4)
    exp_month = models.IntegerField(validators=[MaxValueValidator(12), MinValueValidator(1)])
    exp_year = models.IntegerField(validators=[MinValueValidator(2018), MaxValueValidator(2030)])
    funding = models.CharField(max_length=6)
    holder = models.CharField(max_length=64, validators=[MinLengthValidator(4)])
    number = models.CharField(max_length=16, validators=[MinLengthValidator(16)])
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='cards',
        db_index=True,
        on_delete=models.CASCADE
    )

    def __unicode__(self):
        return "{} {}".format(self.brand, self.number[-4:])

