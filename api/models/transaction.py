# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from api.models.card import Card
from api.models.reservation import Reservation


class Transaction(models.Model):
    object = models.CharField(max_length=12)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    currency = models.CharField(max_length=4)
    customer = models.CharField(max_length=32)
    metadata = models.OneToOneField(
        Reservation,
        related_name='transaction'
    )
    source = models.ForeignKey(
        Card,
        related_name='transactions'
    )
    SUCCEEDED = 's'
    REJECTED = 'r'
    STATUS_CHOICES = (
        (SUCCEEDED, 'succeeded'),
        (REJECTED, 'rejected')
    )
    status = models.CharField(
        max_length=9,
        choices=STATUS_CHOICES,
        default=REJECTED
    )

    def __unicode__(self):
        return self.customer
