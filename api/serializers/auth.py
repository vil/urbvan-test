# -*- coding: utf-8 -*-
"""
Serializer for the user sign in
"""
from __future__ import unicode_literals

from rest_framework import serializers

from api import constants, errors


class SignInSerializer(serializers.Serializer):
    """
    This serializer have the purpose to process the sign in, it does some
    validations.

    ``Attributes:``
        email: required, not blank, maximum length established

        password: required, not blank, maximum and minimum length established
    """
    email = serializers.EmailField(
        max_length=constants.EMAIL_MAX_LENGTH,
        error_messages={
            'required': errors.field_required("email"),
            'blank': errors.field_required("email"),
            'max_length': errors.field_length("email"),
            'invalid': errors.field_invalid("email"),
        }
    )
    password = serializers.CharField(
        min_length=constants.PASSWORD_MIN_LENGTH,
        max_length=constants.PASSWORD_MAX_LENGTH,
        error_messages={
            'required': errors.field_required("contraseña"),
            'blank': errors.field_required("contraseña")
        }
    )
