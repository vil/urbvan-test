# -*- coding: utf-8 -*-
"""
Serializers for cards
"""
from __future__ import unicode_literals

from rest_framework import serializers

from api.models.card import Card
from api.utils.gateway import create_card


class CardSerializer(serializers.ModelSerializer):

    cvv = serializers.CharField(max_length=3, min_length=3, write_only=True)

    class Meta:
        model = Card
        fields = (
            'id',
            'brand',
            'country',
            'cvv',
            'exp_month',
            'exp_year',
            'funding',
            'holder',
            'number',
        )
        read_only_fields = (
            'id',
            'brand',
            'country',
            'funding',
        )
        extra_kwargs = {
            'cvv': {'write_only': True},
            'holder': {'write_only': True},
        }

    def create(self, validated_data):
        gateway_card = create_card(**validated_data)
        gateway_id = gateway_card['id']
        gateway_brand = gateway_card['brand']
        gateway_country = gateway_card['country']
        gateway_type = gateway_card['type']
        validated_data.pop('cvv', '')
        card = Card.objects.create(
            id=gateway_id,
            brand=gateway_brand,
            country=gateway_country,
            funding=gateway_type,
            user_id=self.context['user_id'],
            **validated_data
        )
        return card

    def to_representation(self, instance):
        """
        **Description**
            Hides card number in ``to_representation``
        **Parameters**
            ``instance``: Card instance
        **Returns**
            ``dict``: CardSerializer data with extra information
        """
        data = super(
            CardSerializer, self).to_representation(instance)
        data['number'] = data['number'][-4:]
        return data
