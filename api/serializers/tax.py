# -*- coding: utf-8 -*-
"""
Serializers for cards
"""
from __future__ import unicode_literals

from rest_framework import serializers

from api import errors
from api.models.card import Card
from api.models.tax import TaxProfile


class TaxProfileSerializer(serializers.ModelSerializer):

    card_slug = serializers.StringRelatedField(source='card.__unicode__')

    class Meta:
        model = TaxProfile
        fields = (
            'card',
            'card_slug',
            'legal_name',
            'rfc',
            'state',
            'city',
            'district',
            'neighbourhood',
            'street',
            'street_ext',
            'street_int',
            'postcode',
        )

    def validate_card(self, card):
        if not card.user_id == self.context['user_id']:
            raise serializers.ValidationError(
                errors.field_invalid('card', ' It does not belong to the user logged.')
            )
        if TaxProfile.objects.filter(card_id=card, user_id=self.context['user_id']).exists():
            raise serializers.ValidationError(
                errors.field_invalid('card', ' The user has already registered that card.')
            )
        return card

    def create(self, validated_data):
        profile = TaxProfile.objects.create(
            user_id=self.context['user_id'],
            **validated_data
        )
        return profile
