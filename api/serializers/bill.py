# -*- coding: utf-8 -*-
"""
Serializer for bills
"""
from __future__ import unicode_literals

from datetime import datetime
from rest_framework import serializers

from api.models.bill import Bill
from api.models.card import Card
from api.models.reservation import Reservation
from api.models.tax import TaxProfile
from api.models.transaction import Transaction
from api.utils.bill import generate_xml


def get_tax_profile(user_id, card_id):
    try:
        tax_profile = TaxProfile.objects.values(
            'rfc', 'legal_name'
        ).get(
            user_id=user_id,
            card_id=card_id
        )
    except TaxProfile.DoesNotExist:
        raise serializers.ValidationError(
            'Profile not found'
        )
    else:
        return tax_profile


def init_xml(user_id, folio, rfc, name, rides):
    directory, file_name = generate_xml(folio, rfc, name, rides)
    bill = Bill.objects.create(
        name=file_name,
        file_path="{}/{}".format(directory, file_name),
        user_id=user_id
    )
    return bill


class BillSerializer(serializers.ModelSerializer):
    tax_type = serializers.ChoiceField(
        choices=['monthly', 'card', 'ride'],
        style={'base_template': 'radio.html'},
        write_only=True
    )

    card = serializers.PrimaryKeyRelatedField(
        queryset=Card.objects.all(),
        write_only=True,
        required=False,
        allow_null=True
    )
    ride = serializers.PrimaryKeyRelatedField(
        queryset=Reservation.objects.select_related('pickup', 'dropoff', 'route').all(),
        write_only=True,
        required=False,
        allow_null=True
    )

    class Meta:
        model = Bill
        fields = ('tax_type', 'id', 'name', 'card', 'ride')
        read_only_fields = ('name',)

    def validate(self, attrs):
        tax_type = attrs.get('tax_type', '')

        if tax_type not in ['monthly', 'card', 'ride']:
                raise serializers.ValidationError(
                    'Bad request'
                )
        if (tax_type != 'monthly' and
                (tax_type not in attrs or attrs[tax_type] is None)):
            raise serializers.ValidationError(
                'Bad request'
            )
        return attrs

    def create(self, validated_data):
        tax_type = validated_data.get('tax_type')
        user_id = self.context['user_id']
        now = datetime.now()
        start_date = now.replace(day=1)
        new_month, new_year = (now.month + 1, now.year) if now.month < 12 else (1, now.year + 1)
        end_date = now.replace(day=5, month=new_month, year=new_year)
        if tax_type == 'monthly':
            rides = Reservation.objects.select_related(
                'transaction',
                'transaction__source'
            ).values(
                'pk',
                'seats',
                'transaction__source__id',
                'unit_price'
            ).filter(
                client_id=user_id,
                traveled_at__range=(start_date,end_date),
                billed=False,
                transaction__status=Transaction.SUCCEEDED
            ).order_by(
                'transaction__source__id',
            )
            if not rides.exists():
                raise serializers.ValidationError(
                    'No reservations were found'
                )

            bill_rides = []
            bill_xml = []
            card_active = rides[0]['transaction__source__id']
            for ride in rides:
                if ride['transaction__source__id'] == card_active:
                    bill_rides.append(ride)
                else:
                    tax_profile = get_tax_profile(user_id, card_active)
                    bill = init_xml(user_id, rides[0]['pk'], tax_profile['rfc'],
                             tax_profile['legal_name'], bill_rides)
                    bill_xml.append(bill)
                    card_active = ride['transaction__source__id']
                    bill_rides = [ride]

            tax_profile = get_tax_profile(user_id, card_active)
            bill_xml.append(init_xml(
                user_id, bill_rides[0]['pk'], tax_profile['rfc'],
                tax_profile['legal_name'], bill_rides
            ))
            return bill_xml

        elif tax_type == 'card':
            card_id = validated_data.get('card').pk
            rides = Reservation.objects.select_related(
                'transaction',
                'transaction__source'
            ).values(
                'pk',
                'seats',
                'transaction__source__id',
                'unit_price'
            ).filter(
                client_id=user_id,
                transaction__source_id=card_id,
                traveled_at__range=(start_date, end_date),
                billed=False,
                transaction__status=Transaction.SUCCEEDED
            )
            if not rides.exists():
                raise serializers.ValidationError(
                    'No reservations were found'
                )

            tax_profile = get_tax_profile(user_id, card_id)
            return init_xml(user_id, rides[0]['pk'], tax_profile['rfc'],
                            tax_profile['legal_name'], rides)

        else:
            ride_id = validated_data.get('ride').pk
            try:
                ride = Reservation.objects.select_related(
                    'transaction',
                    'transaction__source'
                ).values(
                    'pk',
                    'seats',
                    'transaction__source__id',
                    'unit_price'
                ).get(
                    pk=ride_id,
                    client_id=user_id,
                    traveled_at__range=(start_date, end_date),
                    billed=False,
                    transaction__status=Transaction.SUCCEEDED
                )

            except Reservation.DoesNotExist:
                raise serializers.ValidationError(
                    'Reservation not found'
                )
            else:
                tax_profile = get_tax_profile(user_id, ride['transaction__source__id'])
                return init_xml(user_id, ride_id, tax_profile['rfc'],
                                tax_profile['legal_name'], [ride])

    def to_representation(self, instance):
        """
        **Description**
            Hides card number in ``to_representation``
        **Parameters**
            ``instance``: Card instance
        **Returns**
            ``dict``: CardSerializer data with extra information
        """
        data = super(
            BillSerializer, self).to_representation(instance)
        if type(instance) == list:
            return {
                'data': [{
                    'pk': bill.pk,
                    'name': bill.name
                } for bill in instance]
            }

        return {
            'data': [data]
        }


class BillListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bill
        fields = ('id', 'name')
        read_only_fields = ('name',)
