# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from api.models.reservation import Reservation, Route, Stop, RouteStop
from api.models.transaction import Transaction


class ReservationAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        return Reservation.objects.select_related(
            'dropoff', 'pickup', 'route',
        ).all()

    def price(self, instance):
        return instance.unit_price * instance.seats

    list_display = ('__unicode__', 'price', 'pickup', 'dropoff')


class RouteStopInline(admin.TabularInline):
    model = RouteStop
    extra = 1


class RouteAdmin(admin.ModelAdmin):
    def stops_account(self, instance):
        return instance.stops.all().count()

    stops_account.short_description = 'Stops'

    inlines = (RouteStopInline,)
    list_display = ('__unicode__', 'stops_account')


class TransactionAdmin(admin.ModelAdmin):
    def reservation(self, instance):
        return instance.metadata.pk
    list_display = ('source', 'reservation', 'status')


admin.site.register(Reservation, ReservationAdmin)
admin.site.register(Route, RouteAdmin)
admin.site.register(Stop)
admin.site.register(Transaction, TransactionAdmin)
