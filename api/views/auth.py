# -*- coding: utf-8 -*-
"""
This file contains the view to handle the authentication methods
"""
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone

from rest_framework import exceptions
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers.auth import SignInSerializer


class SignInView(APIView):
    """
    ``APIView`` for the login

    ``Components:``
        serializer_class = SignInSerializer
    """
    serializer_class = SignInSerializer

    def post(self, request):
        u"""
        **Description**
            This function performs the process of authentication when an user
            provides the correct credentials. First verifies that the given
            information is correct, then if it's correct saves the last login
            date and returned the authorized token with the welcome message.
            If the credentials are wrong it returned the appropriate message
            indicating that they were wrong.
        **Parameters**
            request: The api petition
        **Returns**
            payload: The message appropriate according the result
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = authenticate(username=serializer.validated_data['email'],
                            password=serializer.validated_data['password'])
        if not user:
            raise exceptions.AuthenticationFailed('User not found')

        if not user.is_active:
            raise exceptions.AuthenticationFailed('User is not active')

        user.last_login = timezone.now()
        user.save(update_fields=['last_login'])

        if settings.DEBUG:
            login(request, user)

        token, _ = Token.objects.get_or_create(user=user)
        payload = {
            'name': user.get_full_name(),
            'token': token.key,
        }
        return Response(payload)


class SignOutView(APIView):
    """
    APIView,
    ``Components:``

        permissions: Is Authenticated
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        """
        **Description**
            If the user is connected, this function performs the process to
            perform a successful session closure.
        **Parameters**
            request: The api petition
        **Returns**
            payload: The message appropriate according the result
        """
        key = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[-1]

        if settings.DEBUG and key == '':
            logout(request)
            return Response()
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise Exception
        token.delete()
        return Response()

