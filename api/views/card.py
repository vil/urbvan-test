# -*- coding: utf-8 -*-
"""
This file contains the view to handle card methods
"""
from __future__ import unicode_literals

from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated

from api.models.card import Card
from api.serializers.card import CardSerializer


class CardView(ListCreateAPIView):

    permission_classes = (IsAuthenticated,)
    serializer_class = CardSerializer

    def get_queryset(self):
        return Card.objects.filter(user_id=self.request.user)

    def get_serializer_context(self):
        """
        **Description**
            Returns context with user information
        **Parameters**
            ``None``
        **Returns**
            Dictionary with requesting user instance
        """
        context = super(CardView, self).get_serializer_context()
        context.update({'user_id': self.request.user.id})
        return context
