# -*- coding: utf-8 -*-
"""
This file contains the view to handle taxes methods
"""
from __future__ import unicode_literals

from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated

from api.models.tax import TaxProfile
from api.serializers.tax import TaxProfileSerializer


class TaxProfileView(ListCreateAPIView):

    permission_classes = (IsAuthenticated,)
    serializer_class = TaxProfileSerializer

    def get_queryset(self):
        return TaxProfile.objects.prefetch_related(
            'card',
        ).filter(
            user_id=self.request.user
        )

    def get_serializer_context(self):
        """
        **Description**
            Returns context with user information
        **Parameters**
            ``None``
        **Returns**
            Dictionary with requesting user instance
        """
        context = super(TaxProfileView, self).get_serializer_context()
        context.update({'user_id': self.request.user.id})
        return context
