# -*- coding: utf-8 -*-
"""
This file contains the view to handle bill methods
"""
from __future__ import unicode_literals

import os

from django.conf import settings
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from api.models.bill import Bill
from api.serializers.bill import BillSerializer, BillListSerializer
from api.utils.bill import decrypt


class BillView(ListModelMixin,
               CreateModelMixin,
               GenericViewSet):

    permission_classes = (IsAuthenticated,)
    serializer_class = BillSerializer
    list_serializer_class = BillListSerializer

    def get_queryset(self):
        return Bill.objects.filter(user_id=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return self.list_serializer_class
        return super(BillView, self).get_serializer_class()


    def get_serializer_context(self):
        """
        **Description**
            Returns context with user information
        **Parameters**
            ``None``
        **Returns**
            Dictionary with requesting user instance
        """
        context = super(BillView, self).get_serializer_context()
        context.update({'user_id': self.request.user.id})
        return context


class BillDownloadView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, **kwargs):
        bill = get_object_or_404(Bill, pk=pk)
        file_path = os.path.join(settings.MEDIA_ROOT, bill.file_path.path)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as f:
                xml = decrypt(settings.SECRET_KEY, f.read())
                response = HttpResponse(xml, content_type='application/xml')
                response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
                return response
        raise Http404
