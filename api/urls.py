# -*- coding: utf-8 -*-
"""
API urls.
"""
from __future__ import unicode_literals

from django.conf.urls import include, url

from api.views.auth import SignInView, SignOutView
from api.views.bill import BillView, BillDownloadView
from api.views.card import CardView
from api.views.tax import TaxProfileView


bill_list = BillView.as_view({
    'get': 'list',
    'post': 'create'
})

bills_patterns = ([
    url(r'^$', bill_list, name='bill-list'),
    url(r'^(?P<pk>[0-9]+)/download$', BillDownloadView.as_view(), name='bill-detail')
], 'bills')

cards_patterns = ([
    url(r'^$', CardView.as_view(), name='card-list')
], 'cards')

taxes_patterns = ([
    url(r'^profile/$', TaxProfileView.as_view(), name='profile-list')
], 'taxes')


users_patterns = ([
    url(r'^signin/$', SignInView.as_view(), name='signin'),
    url(r'^signout/$', SignOutView.as_view(), name='signout'),
], 'users')


urlpatterns = [
    url(r'^bills/', include(bills_patterns)),
    url(r'^cards/', include(cards_patterns)),
    url(r'^taxes/', include(taxes_patterns)),
    url(r'^users/', include(users_patterns))
]
