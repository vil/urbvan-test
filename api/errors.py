# -*- coding: utf-8 -*-
"""
Error codes and error messages for *api*
"""
from __future__ import unicode_literals



def field_required(field_name):
    """
    **Description**
        Returns an error message to demand the field.
    **Parameters**
        field_name (str): This string is displayed in the message
    **Returns**
        str: The message with the *field name* in the message
    **Example**
        >>> field_required("contraseña")
        Por favor, ingresar contraseña.
    """
    return "Por favor, ingresar {}.".format(field_name)


def field_length(field_name):
    """
    **Description**
        Returns an error message for the field length.
    **Parameters**
        field_name (str): This string is displayed in the message
    **Returns**
        str: The message with the *field name* in the message
    **Example**
        >>> field_length("email")
        El campo email excede el máximo permitido.
    """
    return "El campo {} excede el máximo permitido.".format(field_name)


def field_invalid(field_name, extras=''):
    """
    **Description**
        Returns an error message for an invalid field.
    **Parameters**
        field_name (str): This string is displayed in the message
    **Returns**
        str: The message with the *field name* in the message
    **Example**
        >>> field_invalid("email")
        El campo email no es válido."""
    return "El campo {} no es válido.{}".format(field_name, extras)