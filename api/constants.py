# -*- coding: utf-8 -*-
"""
This file specifies constants used in the *api*
"""
from __future__ import unicode_literals


PASSWORD_MAX_LENGTH = 100
"""
int: Maximum allowed length for the user password
"""

PASSWORD_MIN_LENGTH = 6
"""
int: Minimum allowed length for the user password
"""

EMAIL_MAX_LENGTH = 64
"""
int: Maximum allowed length for the user email
"""