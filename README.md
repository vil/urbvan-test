# Prueba Urbvan - Gestor de facturas:

[Proyecto de prueba](https://github.com/urbvantransit/backend_prueba_tecnica) realizado por 
Jesús Vila <jvilas1993@gmail.com> utilizando `python` en su versión `2.7`


## Notas

Se recomienda utilizar un ambiente virtual como [virtualenv](https://virtualenv.pypa.io/en/stable/)

Ejecutar archivo `init-server` para instalar dependencias, generar migraciones y crear base
de datos con [SQLite](https://www.sqlite.org/index.html)
```bash
./init-server.sh
```

```bash
python manage.py runserver
```
